// import { showLoading, hideLoading } from 'react-redux-loading'
import { userConstants } from "../constants/user.constants";
import { login, logout, signup } from "../services/login.service";
import { history } from "../helpers/history";
import { alertActions } from "./alert.actions";
import { addDashboard } from "./dashboards";

export function handleLogin(username, password) {
  return dispatch => {
    dispatch(request({ username }));
    login(username, password).then(
      user => {
        dispatch(success(user));
        history.push("/home");
      },
      error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
  function request(user) {
    return { type: userConstants.LOGIN_REQUEST, user };
  }
  function success(user) {
    return { type: userConstants.LOGIN_SUCCESS, user };
  }
  function failure(error) {
    return { type: userConstants.LOGIN_FAILURE, error };
  }
}

export function handleSignUp(
  name,
  lastName,
  username,
  password,
  repeatedPassword
) {
  return dispatch => {
    dispatch(request({ username }));
    signup(name, lastName, username, password, repeatedPassword).then(
      user => {
        dispatch(success(user));
      },
      error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };

  function request(user) {
    return { type: userConstants.LOGIN_REQUEST, user };
  }
  function success(user) {
    return { type: userConstants.LOGIN_SUCCESS, user };
  }
  function failure(error) {
    return { type: userConstants.LOGIN_FAILURE, error };
  }
}

export function handleLogout() {
  history.push("/login");
  logout();
  return { type: userConstants.LOGOUT };
}
