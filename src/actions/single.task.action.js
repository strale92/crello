import { createTask, deleteTaskDelete } from "../helpers/apiCalls";

export const ADD_TASK = "ADD_TASK";
export const EDIT_TASK = "EDIT_TASK";
export const DELETE_TASK = "DELETE_TASK";

export function addTask(task, taskStateId, dashboardId) {
  return {
    type: ADD_TASK,
    task,
    taskStateId,
    dashboardId
  };
}

export function editTask(title, id) {
  return {
    type: EDIT_TASK,
    title,
    id
  };
}

export function deleteTask(id, taskStateId, dashboardId) {
  return {
    type: DELETE_TASK,
    dashboardId,
    taskStateId,
    id
  };
}

export function handleAddTask(title, taskStateId, dashboardId) {
  return dispatch => {
    return createTask(title, taskStateId)
      .then(task => dispatch(addTask(task, taskStateId, dashboardId)))
      .catch(error => alert(error));
  };
}

export function handlEditTask(title, taskId) {
  // return dispatch => {
  //   return editTaskPut(title, taskId)
  //     .then(task => dispatch(editTask(title, taskId)))
  //     .catch(error => alert(error));
  // };
}

export function handleDeleteTask(taskId, taskStateId, dashboardId) {
  return dispatch => {
    return deleteTaskDelete(taskId)
      .then(() => dispatch(deleteTask(taskId, taskStateId, dashboardId)))
      .catch(error => alert(error));
  };
}
