import { showLoading, hideLoading } from "react-redux-loading";

import { recieveDashboards, moveTask } from "./dashboards";
import { getDashboards, putMoveTask } from "../helpers/apiCalls";

export function handleInitialData() {
  return dispatch => {
    dispatch(showLoading());
    return getDashboards().then(dashboards => {
      dispatch(recieveDashboards(dashboards));
      dispatch(hideLoading());
    });
  };
}

export function handleMoveTask(
  dashboardId,
  oldTaskStateId,
  newTaskStateId,
  task
) {
  return dispatch => {
    dispatch(moveTask({ dashboardId, oldTaskStateId, newTaskStateId, task }));
    return putMoveTask(task).catch(() => {
      alert("error occured");
      getDashboards().then(dashboards => {
        dispatch(recieveDashboards(dashboards));
      });
    });
  };
}
