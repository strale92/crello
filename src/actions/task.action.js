import { createTaskState, deleteTaskStateDelete } from "../helpers/apiCalls";

export const ADD_TASKSTATE = "ADD_TASKSTATE";
export const EDIT_TASKSTATE = "EDIT_TASKSTATE";
export const DELETE_TASKSTATE = "DELETE_TASKSTATE";

export function addTaskState(taskState, dashboardId) {
  return {
    type: ADD_TASKSTATE,
    taskState,
    dashboardId
  };
}

export function editTaskState(title, id) {
  return {
    type: EDIT_TASKSTATE,
    title,
    id
  };
}

export function deleteTaskState(id, dashboardId) {
  return {
    type: DELETE_TASKSTATE,
    dashboardId,
    id
  };
}

export function handlAddTaskState(title, dashboardId) {
  return dispatch => {
    return createTaskState(title, dashboardId)
      .then(taskState => dispatch(addTaskState(taskState, dashboardId)))
      .catch(error => alert(error));
  };
}

export function handlEditTaskState(title, taskStateId) {
  // return dispatch => {
  //   return editTaskStatePut(title, taskStateId)
  //     .then(taskState => dispatch(editTaskState(title, taskStateId)))
  //     .catch(error => alert(error));
  // };
}

export function handleDeleteTaskState(taskStateId, dashboardId) {
  return dispatch => {
    return deleteTaskStateDelete(taskStateId)
      .then(() => dispatch(deleteTaskState(taskStateId, dashboardId)))
      .catch(error => alert(error));
  };
}
