import {
  createDashboard,
  editDashboardPut,
  deleteDashboardDelete
} from "../helpers/apiCalls";
import { history } from "../helpers/history";

export const RECIEVE_DASHBOARDS = "RECIEVE_DASHBOARDS";
export const ADD_DASHBOARD = "ADD_DASHBOARD";
export const EDIT_DASHBOARD = "EDIT_DASHBOARD";
export const DELETE_DASHBOARD = "DELETE_DASHBOARD";

export function recieveDashboards(dashboards) {
  return {
    type: RECIEVE_DASHBOARDS,
    dashboards
  };
}
export const MOVE_TASK = "MOVE_TASK";

export function moveTask(object) {
  return {
    type: MOVE_TASK,
    object
  };
}

export function addDashboard(dashboard) {
  return {
    type: ADD_DASHBOARD,
    dashboard
  };
}

export function editDashboard(title, id) {
  return {
    type: EDIT_DASHBOARD,
    title,
    id
  };
}

export function deleteDashboard(id) {
  return {
    type: DELETE_DASHBOARD,

    id
  };
}

export function handlAddDashboard(title, userId) {
  return dispatch => {
    return createDashboard(title, userId)
      .then(dashboard => dispatch(addDashboard(dashboard)))
      .catch(error => alert(error));
  };
}

export function handlEditDashboard(title, dashboardId) {
  return dispatch => {
    return editDashboardPut(title, dashboardId)
      .then(dashboard => dispatch(editDashboard(title, dashboardId)))
      .catch(error => alert(error));
  };
}

export function handleDeleteDashboard(dashboardId) {
  return dispatch => {
    return deleteDashboardDelete(dashboardId)
      .then(() => {
        dispatch(deleteDashboard(dashboardId));
        history.push("/home");
      })
      .catch(error => alert(error));
  };
}
