import axios from "axios";
// import { history } from '../helpers/history'

export function login(username, password) {
  return (
    axios
      .post(
        "http://localhost:8080/users/login",
        {
          username: username,
          password: password
        },
        { withCredentials: true }
      )
      // .then(handleResponse)
      .then(user => {
        localStorage.setItem("user", JSON.stringify(user.data));
      })
  );
}

export function signup(name, lastName, username, password, repeatedPassword) {
  return (
    axios
      .post(
        "http://localhost:8080/users/register",
        {
          name,
          lastName,
          username,
          password,
          repeatedPassword
        },
        { withCredentials: true }
      )
      // .then(handleResponse)
      .then(user => {
        localStorage.setItem("user", JSON.stringify(user.data));
        return user.data;
      })
      .then(user => {
        user.dashboard = createInitialDashboard();
        return user;
      })
  );
}

function createInitialDashboard() {
  return axios
    .post(
      "http://localhost:8080/dashboards",
      {
        title: "MyFirstDashboard",
        userId: JSON.parse(localStorage.getItem("user")).id
      },
      { withCredentials: true }
    )
    .then(response => {
      return response.data;
    });
}

export function logout() {
  axios
    .get("http://localhost:8080/users/logout", {
      withCredentials: true
    })
    .then(() => {
      localStorage.removeItem("user");
    })
    .catch(() => {
      localStorage.removeItem("user");
    });
}
