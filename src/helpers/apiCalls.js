import axios from "axios";

let credentials = {
  withCredentials: true,
  credentials: "same-origin"
};

export function getDashboards() {
  const DASHBOARDS_URL = "http://localhost:8080/users/dashboards";

  function makeDashboardUrl(dashboardID) {
    return `http://localhost:8080/dashboards/${dashboardID}/task-states`;
  }

  function makeTaskStateUrl(taskStateId) {
    return `http://localhost:8080/task-states/${taskStateId}/tasks`;
  }

  function getDashboardsInfo(dashboardsInfo) {
    let dashboardUrls = [];

    let accumulatedData = {};

    // console.log("Dashboards info", dashboardsInfo);

    dashboardsInfo.data.forEach(dashboard => {
      dashboardUrls.push(makeDashboardUrl(dashboard.id));
    });

    accumulatedData.dashboardsInfo = dashboardsInfo.data;
    accumulatedData.dashboardUrls = dashboardUrls;

    return axios
      .all(dashboardUrls.map(url => axios.request(url, credentials)))
      .then(res => {
        accumulatedData.taskStates = res.map(item => item.data);

        let taskStateUrls = [];
        accumulatedData.taskStates.map(tskStates =>
          tskStates.map(tskSt => taskStateUrls.push(makeTaskStateUrl(tskSt.id)))
        );
        accumulatedData.taskStateUrls = taskStateUrls;

        return accumulatedData;
      })
      .catch(error => {
        if (error.response.status === 404) {
          accumulatedData.taskStates = [];
          return accumulatedData;
        }
      });
  }

  function getTaskStates(res) {
    let accumulatedData = res;

    if (res.taskStateUrls === undefined) {
      return accumulatedData;
    } else {
      return axios
        .all(
          res.taskStateUrls.map(url =>
            axios.request(url, credentials).catch(error => {
              if (error.response.status === 404) {
                return accumulatedData;
              }
            })
          )
        )
        .then(resp => {
          accumulatedData.tasks = resp.map(tasks => tasks.data);

          return accumulatedData;
        });
    }
  }

  // imamo dashboard, taskstates.
  return axios
    .get(DASHBOARDS_URL, credentials)
    .then(getDashboardsInfo)
    .then(getTaskStates)
    .then(response => organizeData(response));
}

function organizeData(dashboardsInfo) {
  console.log(dashboardsInfo);
  const mergedTasks = [];
  const mergedTaskStates = [];

  dashboardsInfo.dashboardsInfo.forEach(
    dashboard => (dashboard.taskStates = [])
  );

  dashboardsInfo.tasks.forEach(taskArr =>
    taskArr.forEach(task => mergedTasks.push(task))
  );

  dashboardsInfo.taskStates.forEach(taskstArr =>
    taskstArr.forEach(taskState => {
      taskState.tasks = [];
      mergedTaskStates.push(taskState);
    })
  );

  mergedTasks.forEach(task => {
    mergedTaskStates.forEach(taskState => {
      if (task.taskStateId === taskState.id) {
        taskState.tasks.push(task);
      }
    });
  });

  mergedTaskStates.forEach(taskState => {
    dashboardsInfo.dashboardsInfo.forEach(dashboard => {
      if (taskState.dashboardId === dashboard.id) {
        dashboard.taskStates.push(taskState);
      }
    });
  });

  return dashboardsInfo.dashboardsInfo;
}

// function organizeData(dashboardsInfo) {
//   console.log(dashboardsInfo);
//   let data = dashboardsInfo.dashboardsInfo.map((dashboard, i) => {
//     if (dashboardsInfo.taskStates[i] === undefined) {
//       return {
//         ...dashboard,
//         taskStates: []
//       };
//     }
//     return {
//       ...dashboard,
//       taskStates: [...dashboardsInfo.taskStates[i]]
//     };
//   });
//   let finalData = data.map((dashboard, i) => {
//     if (dashboard.taskStates[i] === undefined) {
//       return {
//         ...dashboard,
//         taskStates: []
//       };
//     }
//     let tskSt = dashboard.taskStates.map((taskState, k) => {
//       if (!dashboardsInfo.tasks[k]) {
//         return {
//           ...taskState,
//           tasks: []
//         };
//       }
//       return {
//         ...taskState,
//         tasks: [...dashboardsInfo.tasks[k]]
//       };
//     });

//     return {
//       ...dashboard,
//       taskStates: [...tskSt]
//     };
//   });

//   return finalData;
// }

export function putMoveTask(task) {
  return axios
    .put(`http://localhost:8080/tasks/${task.id}`, task, credentials)
    .then(task => {
      return task.data;
    });
}

export function createDashboard(title, userId) {
  return axios
    .post(
      "http://localhost:8080/dashboards",
      {
        title,
        userId
      },
      { withCredentials: true }
    )
    .then(dashboard => dashboard.data);
}

export function editDashboardPut(title, dashboardId) {
  return axios.put(
    `http://localhost:8080/dashboards/${dashboardId}`,
    {
      title,
      id: dashboardId
    },
    { withCredentials: true }
  );
}

export function deleteDashboardDelete(dashboardId) {
  return axios.delete(`http://localhost:8080/dashboards/${dashboardId}`, {
    withCredentials: true
  });
}

export function createTaskState(title, dashboardId) {
  return axios
    .post(
      "http://localhost:8080/task-states",
      {
        title,
        dashboardId
      },
      { withCredentials: true }
    )
    .then(taskState => taskState.data);
}

export function deleteTaskStateDelete(taskStateId) {
  return axios.delete(`http://localhost:8080/task-states/${taskStateId}`, {
    withCredentials: true
  });
}

export function createTask(title, taskStateId) {
  return axios
    .post(
      "http://localhost:8080/tasks",
      {
        title,
        description: `${title}'s description`,
        taskStateId
      },
      { withCredentials: true }
    )
    .then(task => task.data);
}

export function deleteTaskDelete(taskId) {
  return axios.delete(`http://localhost:8080/tasks/${taskId}`, {
    withCredentials: true
  });
}
