import { combineReducers } from "redux";
import { loadingBarReducer } from "react-redux-loading";
import { alert } from "./alert.reducer";
import { authentication } from "./authentication.reducer";
import dashboards from "./dashboardsReducer";

export default combineReducers({
  alert,
  dashboards,
  authentication,
  loadingBar: loadingBarReducer
});
