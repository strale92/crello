import {
  RECIEVE_DASHBOARDS,
  MOVE_TASK,
  ADD_DASHBOARD,
  EDIT_DASHBOARD,
  DELETE_DASHBOARD
} from "../actions/dashboards";

import {
  ADD_TASKSTATE,
  EDIT_TASKSTATE,
  DELETE_TASKSTATE
} from "../actions/task.action";

import {
  ADD_TASK,
  EDIT_TASK,
  DELETE_TASK
} from "../actions/single.task.action";

export default function dashboards(state = [], action) {
  switch (action.type) {
    case RECIEVE_DASHBOARDS:
      return [...state, ...action.dashboards];
    case MOVE_TASK:
      const array = state.slice();
      const array2 = [...state];
      console.log("arr", array);
      console.log("arr2", array2);
      //filter dashboard
      let dashboard = array.filter(
        dashboard => dashboard.id === action.object.dashboardId
      )[0];

      // filter task states
      let oldTaskState = dashboard.taskStates.filter(
        taskState => taskState.id === action.object.oldTaskStateId
      )[0];
      let newTaskState = dashboard.taskStates.filter(
        taskState => taskState.id === action.object.newTaskStateId
      )[0];

      // filter task which changes
      const changingTask = oldTaskState.tasks.filter(
        task => task.id === action.object.task.id
      )[0];

      //remove task from old task list
      oldTaskState.tasks = oldTaskState.tasks.filter(
        task => task.id !== changingTask.id
      );

      //change task reference from old to new array
      changingTask.taskStateId = action.object.newTaskStateId;

      // add task to new array
      newTaskState.tasks.push(changingTask);

      let pos = array2
        .map(function(e) {
          return e.id;
        })
        .indexOf(action.object.dashboardId);

      array2[pos] = dashboard;

      return array2;
    case ADD_DASHBOARD:
      let dash = action.dashboard;
      dash.taskStates = [];
      return [...state, dash];
    case EDIT_DASHBOARD:
      let newState = [...state];
      newState.forEach(dashboard => {
        if (dashboard.id === action.id) {
          dashboard.title = action.title;
        }
      });
      return newState;
    case DELETE_DASHBOARD:
      let filteredState = [...state].filter(
        dashboard => dashboard.id !== action.id
      );
      return filteredState;
    case ADD_TASKSTATE:
      let index = state.findIndex(
        dashboard => dashboard.id === action.dashboardId
      );

      let taskState = action.taskState;
      taskState.tasks = [];
      let updatedState = [...state];

      updatedState[index].taskStates.push(taskState);
      return updatedState;
    case DELETE_TASKSTATE:
      let dashIndex = state.findIndex(
        dashboard => dashboard.id === action.dashboardId
      );

      let tempState = [...state];

      let dashboardWithTaskStateForRemoving = tempState[dashIndex];

      dashboardWithTaskStateForRemoving.taskStates = dashboardWithTaskStateForRemoving.taskStates.filter(
        taskState => taskState.id !== action.id
      );

      tempState[dashIndex] = dashboardWithTaskStateForRemoving;
      return tempState;
    case ADD_TASK:
      let copyArray = state.slice();
      index = state.findIndex(dashboard => dashboard.id === action.dashboardId);

      let taskStateIndex = state[index].taskStates.findIndex(
        taskState => taskState.id === action.taskStateId
      );

      copyArray[index].taskStates[taskStateIndex].tasks.push(action.task);

      return copyArray;
    case DELETE_TASK:
      copyArray = state.slice();
      index = state.findIndex(dashboard => dashboard.id === action.dashboardId);
      taskStateIndex = state[index].taskStates.findIndex(
        taskState => taskState.id === action.taskStateId
      );
      console.log("i", index);
      console.log("tsi", taskStateIndex);
      console.log("cpi", copyArray[index]);
      console.log("cpi tski", copyArray[index].taskStates[taskStateIndex]);
      copyArray[index].taskStates[taskStateIndex].tasks = copyArray[
        index
      ].taskStates[taskStateIndex].tasks.filter(task => task.id !== action.id);

      return copyArray;
    default:
      return state;
  }
}
