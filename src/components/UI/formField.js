import React from "react";

const FormField = ({ id, formdata, change }) => {
  const showError = () => {
    let errorMessage = (
      <div className="error">
        {formdata.validation && !formdata.valid
          ? formdata.validationMessage
          : null}
      </div>
    );

    return errorMessage;
  };

  const renderTemplate = () => {
    let formTemplate = (
      <div>
        <input
          className="formField"
          {...formdata.config}
          value={formdata.value}
          onChange={event => change({ event, id })}
        />
        {showError()}
      </div>
    );

    return formTemplate;
  };

  return <div>{renderTemplate()}</div>;
};

export default FormField;
