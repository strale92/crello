import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import { connect } from "react-redux";
import { handleLogout } from "../../actions/login.actions";
import { Link } from "react-router-dom";
import Avatar from "@material-ui/core/Avatar";

class Header extends Component {
  render() {
    let user = JSON.parse(localStorage.getItem("user"));
    return (
      <AppBar
        position="static"
        style={{
          backgroundColor: "#fff",
          boxShadow: "none",
          padding: "10px 0"
        }}
      >
        <Toolbar style={{ display: "flex" }}>
          <div style={{ flexGrow: 1 }}>
            <div className="logo" style={{ color: "#373737" }}>
              <Link className="logo" to="/home">
                Crello
              </Link>
            </div>
          </div>

          <Button style={{ color: "#373737", margin: 2 }} color="inherit">
            <Link to="/home"> Home</Link>
          </Button>
          <Link
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center"
            }}
            to="/home/profile"
          >
            <Button style={{ margin: 2, padding: "0 5px" }}>
              <Avatar
                style={{
                  backgroundColor: "#DCD0C0",
                  width: "35px",
                  height: "35px",
                  margin: "2px 8px 2px 2px"
                }}
              >
                {user.name.charAt(0).toUpperCase()}
              </Avatar>{" "}
              Profile
            </Button>
          </Link>
          <Button
            style={{
              color: "#373737",
              margin: 2
            }}
            color="inherit"
            onClick={() => this.props.dispatch(handleLogout())}
          >
            Log Out
          </Button>
        </Toolbar>
      </AppBar>
    );
  }
}

export default connect()(Header);
