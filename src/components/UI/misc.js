export const validate = element => {
  let error = [true, ""];

  if (element.config.field === "username") {
    const valid = !(element.value.toUpperCase().indexOf("CRELLO") > -1);
    const message = `${!valid ? "Username can not contain crello" : ""}`;
    error = !valid ? [valid, message] : error;
  }

  if (element.validation.required) {
    const valid =
      element.value.trim() !== "" && element.value.trim().length > 2;
    const message = `${
      !valid ? "This field must be min 3 characters long" : ""
    }`;
    error = !valid ? [valid, message] : error;
  }

  return error;
};

export const responseLoop = response => {
  let data = [];
  response.forEach(singleResp => {
    data.push(singleResp);
  });
  return data;
};
