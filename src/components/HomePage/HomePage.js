import React, { Component } from "react";
import { connect } from "react-redux";
import { handleInitialData } from "../../actions/shared";
import { Route, Link, Switch } from "react-router-dom";
import { handlAddDashboard } from "../../actions/dashboards";
import Header from "../UI/Header";
import Dashboard from "./Dashboard/Dashboard";
import Profile from "../Profile/index";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import DashboardButton from "../Dashboard-button";

class HomePage extends Component {
  state = {
    showEditForm: false,
    tempTitle: ""
  };

  componentDidMount() {
    this.props.dispatch(handleInitialData());
  }

  handleChange = e => {
    this.setState({ tempTitle: e.target.value });
  };

  handleClickAway = () => {
    this.setState({
      showEditForm: false
    });
  };

  showEditForm = () => {
    this.setState(prevState => ({
      showEditForm: !prevState.showEditForm
    }));
  };

  addNew(event) {
    event.preventDefault();
    // let taskResp;
    this.props.dispatch(
      handlAddDashboard(this.state.tempTitle, this.props.user.id)
    );
    this.setState({ showEditForm: false, tempTitle: "" });
  }

  showDashboards = () => {
    return (
      <div className="dashboards-list">
        {this.props.user ? (
          <div className="welcome">
            Welcome: {this.props.user.name} {this.props.user.lastName}{" "}
          </div>
        ) : null}
        <div className="dashboard-container">
          {this.props.dashboards
            ? this.props.dashboards.map(dashboard => (
                <DashboardButton key={dashboard.id} dashboard={dashboard} />
              ))
            : null}
          {this.state.showEditForm ? (
            <ClickAwayListener onClickAway={this.handleClickAway}>
              <form
                className="dashboard_card-form"
                onSubmit={event => this.addNew(event)}
              >
                <input
                  type="text"
                  value={this.state.tempTitle}
                  onChange={e => this.handleChange(e)}
                />
                <button
                  className="add-btn"
                  type="submit"
                  disabled={this.state.tempTitle.length < 3}
                >
                  <i className="fas fa-plus" />
                </button>
              </form>
            </ClickAwayListener>
          ) : (
            <div
              onClick={() => this.showEditForm()}
              className="add-new_dashboard_card"
            >
              <i className="fas fa-plus edt" /> Add new dashboard
            </div>
          )}
        </div>
      </div>
    );
  };

  render() {
    const { match } = this.props;
    return (
      <div>
        <Header />

        <Route
          exact
          path={`${match.url}`}
          render={() => this.showDashboards()}
        />
        <Route path={`/home/profile`} component={Profile} />
        <Route path={`/home/dashboards/:dashboardId`} component={Dashboard} />
      </div>
    );
  }
}

function mapStateToProps(state, { match }) {
  const { dashboards, authentication } = state;
  const dash = Object.values(dashboards);
  const { user } = authentication;
  return {
    dashboards,
    user,
    match
  };
}

export default connect(mapStateToProps)(HomePage);
