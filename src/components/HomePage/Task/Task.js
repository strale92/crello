import React, { Component } from "react";
import { Draggable } from "react-beautiful-dnd";
import styled from "styled-components";
import { connect } from "react-redux";
import { handleDeleteTask } from "../../../actions/single.task.action";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";

const Delete = styled.div`
  position: absolute;
  display: none;
  top: 0px;
  right: 0px;
  width: 12px;
  height: 15px;
  font-size: 13px;
  cursor: pointer;
  color: #fff;
  font-weight: bold;
  background: crimson;
  border-radius: 3px;
`;

const Container = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  color: #17394d;
  background-color: ${props => (props.isDragging ? "lightgreen" : "#fff")};
  justify-content: start;
  width: 290px;
  min-height: 20px;
  text-align: left;
  margin-bottom: ${props => (props.last ? "2px" : "8px")}
  box-shadow: 0 1px 0 rgba(9, 45, 66, 0.25);
  border-radius: 3px;
  padding: 4px;
  &:hover ${Delete} {
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

const Handle = styled.div`
  width: 20px;
  height: 20px;
  background-color: inherit;
  border-radius: 4px;
  margin-right: 8px;
  text-align: center;
`;

const Card = styled.div`
  display: block;
  cursor: pointer;
  font-size: 15px;
  line-height: 20px;
  font-weight: 400;
  overflow: hidden;
  width: 240px
  word-wrap: break-word;
`;

class Task extends Component {
  state = {
    open: false,
    showDescriptionForm: false,
    description: this.props.desc
  };

  // componentDidMount() {
  //   this.onChange(this.props.desc);
  // }

  handleClickAway = () => {
    this.setState({
      showDescriptionForm: false
    });
  };

  handleChange = e => {
    this.setState({ description: e.target.value });
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  delete = id => {
    this.props.dispatch(
      handleDeleteTask(id, +this.props.taskStateId, +this.props.dashboardId)
    );
  };
  render() {
    return (
      <div>
        <Draggable draggableId={this.props.id + ""} index={this.props.index}>
          {(provided, snapshot) => (
            <Container
              {...provided.draggableProps}
              ref={provided.innerRef}
              isDragging={snapshot.isDragging}
              last={this.props.last}
            >
              <Handle {...provided.dragHandleProps}>
                <i className="fas fa-arrows-alt" />
              </Handle>

              {this.props.id ? (
                <Card onClick={this.handleClickOpen}>{this.props.title}</Card>
              ) : null}
              <Delete onClick={() => this.delete(this.props.id)}>X</Delete>
            </Container>
          )}
        </Draggable>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {`${this.props.title}`}
          </DialogTitle>
          <DialogContent>
            <strong>Description:</strong>
            {this.state.showDescriptionForm ? (
              <div>{this.state.description}</div>
            ) : (
              <ClickAwayListener onClickAway={this.handleClickAway}>
                <form
                  className=""
                  // onSubmit={event => this.addNew(event)}
                >
                  <textarea
                    rows="3"
                    value={this.state.description}
                    onChange={e => this.handleChange(e)}
                  />
                  <button className="" type="submit">
                    <i className="fas fa-plus" />
                  </button>
                </form>
              </ClickAwayListener>
            )}
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Disagree
            </Button>
            <Button onClick={this.handleClose} color="primary" autoFocus>
              Agree
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default connect()(Task);
