import React, { Component } from "react";
import { Button } from "@material-ui/core";
import TaskStates from "../TaskStates/TaskStates";
import { connect } from "react-redux";
import { handleMoveTask } from "../../../actions/shared";
import { handleDeleteDashboard } from "../../../actions/dashboards";
import {
  handlAddTaskState,
  handleDeleteTaskState
} from "../../../actions/task.action";
import { DragDropContext } from "react-beautiful-dnd";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";

// import { history } from "../../../helpers/history";

class Dashboard extends Component {
  state = {
    open: false,
    showEditForm: false,
    tempTitle: "",
    elements: []
  };

  handleChange = e => {
    this.setState({ tempTitle: e.target.value });
  };

  handleClickAway = () => {
    this.setState({
      showEditForm: false
    });
  };

  showEditForm = () => {
    this.setState(prevState => ({
      showEditForm: !prevState.showEditForm
    }));
  };

  addNew(event) {
    event.preventDefault();
    this.props.dispatch(
      handlAddTaskState(this.state.tempTitle, +this.props.dashboardId)
    );
    this.setState({ showEditForm: false, tempTitle: "" });
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  delete = () => {
    this.props.dispatch(handleDeleteDashboard(this.props.dashboardId));
  };

  deleteTaskState = id => {
    this.props.dispatch(handleDeleteTaskState(id, +this.props.dashboardId));
  };

  isOverflown(element) {
    if (element === undefined) {
      return false;
    }
    return (
      element.scrollHeight > element.clientHeight ||
      element.scrollWidth > element.clientWidth
    );
  }

  showTaskLists = () => (
    <div className="taskstates_container">
      {this.props.taskStates
        ? this.props.taskStates.map((taskState, i) => (
            <div
              key={taskState.id}
              className={
                taskState.tasks.length > 20
                  ? "taskstates-overflow"
                  : "taskstates"
              }
            >
              <div className="taskstates_header">{taskState.title}</div>
              <div
                className="delete-taskState"
                onClick={() => this.deleteTaskState(taskState.id)}
              >
                X
              </div>
              <TaskStates
                taskStateId={taskState.id}
                dashboardId={+this.props.dashboardId}
              />
            </div>
          ))
        : null}
      {this.state.showEditForm ? (
        <ClickAwayListener onClickAway={this.handleClickAway}>
          <form
            className="taskstate_card-form"
            onSubmit={event => this.addNew(event)}
          >
            <input
              type="text"
              value={this.state.tempTitle}
              onChange={e => this.handleChange(e)}
            />
            <button
              className="add-btn-tasklist"
              type="submit"
              disabled={this.state.tempTitle.length < 3}
            >
              <i className="fas fa-plus" />
            </button>
          </form>
        </ClickAwayListener>
      ) : (
        <div
          onClick={() => this.showEditForm()}
          className="add-new_task-list_card"
        >
          <i className="fas fa-plus edt" /> Add new task list
        </div>
      )}
    </div>
  );
  onDragEnd = result => {
    const { destination, source, draggableId } = result;
    const { dashboard } = this.props;

    if (!destination) {
      return;
    }

    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return;
    }

    const start = dashboard.taskStates.filter(
      taskState => taskState.id === +source.droppableId
    )[0];
    const end = dashboard.taskStates.filter(
      taskState => taskState.id === +destination.droppableId
    )[0];

    let task = start.tasks.filter(task => task.id === +draggableId)[0];

    if (start === end) {
      return;
    }

    this.props.dispatch(
      handleMoveTask(
        dashboard.id,
        +source.droppableId,
        +destination.droppableId,
        task
      )
    );
  };
  render() {
    console.log(this.state);
    return (
      <div>
        <div className="dashboard_title">
          <Button
            size="small"
            style={{ backgroundColor: "rgb(55, 55, 55)", color: "#fff" }}
            onClick={() => this.props.history.goBack()}
          >
            <i className="fas fa-chevron-left back-btn" /> back to dashboards
          </Button>
          <span className="dashboard_name">
            You are currently in: {this.props.dashboardName}
          </span>
          <Button size="small" color="secondary" onClick={this.handleClickOpen}>
            <i className="far fa-trash-alt delete-btn" /> Delete this dashboard
          </Button>
        </div>
        <DragDropContext onDragEnd={this.onDragEnd}>
          <div>
            <div>{this.showTaskLists()}</div>
          </div>
        </DragDropContext>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"Are you sure?"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              If you delete dashboard you will delete all task lists and task it
              contains
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              No, take me back
            </Button>
            <Button onClick={this.delete} color="primary" autoFocus>
              Yes, I am sure
            </Button>
          </DialogActions>
        </Dialog>
        {/* <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"Are you sure?"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              If you delete task list you will delete all tasks it
              contains
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              No, take me back
            </Button>
            <Button onClick={this.deleteTaskState} color="primary" autoFocus>
              Yes, I am sure
            </Button>
          </DialogActions>
        </Dialog> */}
      </div>
    );
  }
}

function mapStateToProps({ dashboards }, { match, history }) {
  const { dashboardId } = match.params;
  if (dashboards.length === 0) {
    return {
      taskStates: null
    };
  }

  // const dashboard = Object.values(dashboards);

  const dashboard = dashboards.filter(dashboard => {
    return dashboard.id === +dashboardId;
  })[0];
  console.log(dashboard);

  return {
    taskStates: [...dashboard.taskStates],
    dashboardName: dashboard.title,
    dashboardId,
    dashboard: dashboard,
    history
  };
}

export default connect(mapStateToProps)(Dashboard);
