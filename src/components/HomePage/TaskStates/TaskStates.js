import React, { Component } from "react";
import { connect } from "react-redux";
import Task from "../Task/Task";
import { Droppable } from "react-beautiful-dnd";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import { handleAddTask } from "../../../actions/single.task.action";

class TaskStates extends Component {
  state = {
    showEditForm: false,
    tempTitle: ""
  };

  handleChange = e => {
    this.setState({ tempTitle: e.target.value });
  };

  handleClickAway = () => {
    this.setState({
      showEditForm: false
    });
  };

  showEditForm = () => {
    this.setState(prevState => ({
      showEditForm: !prevState.showEditForm
    }));
  };

  addNew(event) {
    event.preventDefault();
    this.props.dispatch(
      handleAddTask(
        this.state.tempTitle,
        +this.props.taskStateId,
        +this.props.dashboardId
      )
    );
    this.setState({ showEditForm: false, tempTitle: "" });
  }

  loadTasks = () => {
    return this.props.tasks ? (
      <div>
        <Droppable droppableId={this.props.taskStateId + ""}>
          {(provided, snapshot) => (
            <div
              className="holder"
              ref={provided.innerRef}
              {...provided.droppableProps}
            >
              {this.props.tasks.map((task, index) => (
                <Task
                  key={task.id}
                  id={task.id}
                  taskStateId={task.taskStateId}
                  dashboardId={this.props.dashboardId}
                  desc={task.description}
                  title={task.title}
                  index={index}
                  last={this.props.tasks.length === index + 1}
                />
              ))}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
        {this.state.showEditForm ? (
          <ClickAwayListener onClickAway={this.handleClickAway}>
            <form
              className="tasks_card-form"
              onSubmit={event => this.addNew(event)}
            >
              <input
                type="text"
                value={this.state.tempTitle}
                onChange={e => this.handleChange(e)}
              />
              <button
                className="add-btn-tasklist"
                type="submit"
                disabled={this.state.tempTitle.length < 3}
              >
                <i className="fas fa-plus" />
              </button>
            </form>
          </ClickAwayListener>
        ) : (
          <div onClick={() => this.showEditForm()} className="addnew">
            <i className="fas fa-plus edt" /> Add new task
          </div>
        )}
      </div>
    ) : null;
  };

  render() {
    return <div>{this.loadTasks()}</div>;
  }
}

function mapStateToProps({ dashboards }, ids) {
  const { taskStateId } = ids;
  const { dashboardId } = ids;

  if (dashboards.length === 0) {
    return {
      tasks: null
    };
  }

  let dashboard = Object.values(dashboards).filter(dashboard => {
    return dashboard.id === dashboardId;
  });

  let taskStates = [...dashboard[0].taskStates];

  let taskState = taskStates.filter(taskState => {
    return taskState.id === taskStateId;
  });
  return {
    tasks: [...taskState[0].tasks],
    dashboardId,
    taskStateId
  };
}

export default connect(mapStateToProps)(TaskStates);
