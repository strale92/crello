import React, { Component } from "react";
import { Link } from "react-router-dom";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import { handlEditDashboard } from "../../actions/dashboards";
import { connect } from "react-redux";

class DashboardButton extends Component {
  state = {
    showDashboardEditForm: false,
    editTitle: this.props.dashboard.title
  };

  handleClickAway = () => {
    this.setState({
      showDashboardEditForm: false
    });
  };

  edit(event) {
    event.preventDefault();
    // let taskResp;
    if (this.props.dashboard.title === this.state.editTitle) {
      this.setState({ showDashboardEditForm: false });
    } else {
      this.props.dispatch(
        handlEditDashboard(this.state.editTitle, this.props.dashboard.id)
      );
      this.setState({ showDashboardEditForm: false });
    }
  }

  showEditForm = () => {
    this.setState(prevState => ({
      showDashboardEditForm: !prevState.showDashboardEditForm
    }));
  };

  handleChange = e => {
    this.setState({ editTitle: e.target.value });
  };

  render() {
    return (
      <div className="clickable-link-dash">
        {this.state.showDashboardEditForm ? (
          <ClickAwayListener onClickAway={this.handleClickAway}>
            <form
              className="dashboard_card-form"
              onSubmit={event => this.edit(event)}
            >
              <input
                type="text"
                value={this.state.editTitle}
                onChange={e => this.handleChange(e)}
              />
              <button
                className="edit-btn"
                type="submit"
                disabled={this.state.editTitle.length < 3}
              >
                <i className="fas fa-pen" />
              </button>
            </form>
          </ClickAwayListener>
        ) : (
          <div className="dashboard_card">
            <Link
              key={this.props.dashboard.id}
              to={`home/dashboards/${this.props.dashboard.id}`}
            >
              <div>
                <i
                  className="fas fa-clipboard-list"
                  style={{ fontSize: "120%", margin: "3px" }}
                />{" "}
                {this.state.editTitle}
              </div>
            </Link>
            <div className="pen-edit" onClick={() => this.showEditForm()}>
              <i className="fas fa-pen" />
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default connect()(DashboardButton);
