import React, { Component } from "react";
import { history } from "../helpers/history";
import { alertActions } from "../actions/alert.actions";
import { connect } from "react-redux";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import { PrivateRoute } from "./PrivateRoute";
import HomePage from "./HomePage/HomePage";
import LoginPage from "./LoginPage/LoginPage";
import { LoadingBar } from "react-redux-loading";
import SignUp from "./Sign Up/SignUp";

class App extends Component {
  constructor(props) {
    super(props);

    const { dispatch } = this.props;
    history.listen((location, action) => {
      // clear alert on location change
      dispatch(alertActions.clear());
    });
  }
  showAlert = () => <div className="error-msg">{this.props.alert.message}</div>;

  render() {
    return (
      <div className="App">
        {this.props.alert.message ? this.showAlert() : null}
        <LoadingBar />
        <Router>
          <Switch>
            <PrivateRoute path="/home" component={HomePage} />
            {localStorage.getItem("user") ? (
              <Redirect to="/home" />
            ) : (
              <Route path="/login" component={LoginPage} />
            )}
            {localStorage.getItem("user") ? (
              <Redirect to="/home" />
            ) : (
              <Route path="/signup" component={SignUp} />
            )}
            <PrivateRoute
              render={() => (
                <div
                  style={{
                    height: "100vh",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    fontSize: "500%"
                  }}
                >
                  404
                </div>
              )}
            />
          </Switch>
        </Router>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { alert, authentication } = state;
  const { user } = authentication;
  return {
    alert,
    authentication
  };
}

export default connect(mapStateToProps)(App);
