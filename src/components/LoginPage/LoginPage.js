import React, { Component } from "react";
import { validate } from "../UI/misc";
import FormField from "../UI/formField";
import Button from "@material-ui/core/Button";
import { connect } from "react-redux";
import { handleLogin } from "../../actions/login.actions";
import { Link } from "react-router-dom";

class Login extends Component {
  state = {
    // formError: false,
    // formSuccess: "",
    formdata: {
      username: {
        element: "input",
        value: "",
        config: {
          name: "username_input",
          type: "text",
          placeholder: "Enter your username"
        },
        validation: {
          required: true
        },
        valid: false,
        validationMessage: ""
      },
      password: {
        element: "input",
        value: "",
        config: {
          name: "password_input",
          type: "password",
          placeholder: "Enter your password"
        },
        validation: {
          required: true
        },
        valid: false,
        validationMessage: ""
      }
    }
  };

  updateForm(element) {
    const newFormdata = { ...this.state.formdata };
    const newElement = { ...newFormdata[element.id] };

    newElement.value = element.event.target.value;

    let validData = validate(newElement);

    newElement.valid = validData[0];
    newElement.validationMessage = validData[1];

    newFormdata[element.id] = newElement;

    this.setState({ formError: false, formdata: newFormdata });
  }

  submitForm(event) {
    event.preventDefault();

    let dataToSubmit = {};
    let formIsValid = true;

    for (let key in this.state.formdata) {
      dataToSubmit[key] = this.state.formdata[key].value;
      formIsValid = this.state.formdata[key].valid && formIsValid;
    }

    if (formIsValid) {
      this.props.dispatch(
        handleLogin(dataToSubmit.username, dataToSubmit.password)
      );
    }
  }

  render() {
    return (
      <div className="landing">
        <h1>Crello</h1>
        <div className="container">
          <div className="signin_wrapper" style={{ margin: "100px" }}>
            <form onSubmit={event => this.submitForm()}>
              <h2>Please Login</h2>
              <FormField
                className="formField"
                id={"username"}
                formdata={this.state.formdata.username}
                change={element => this.updateForm(element)}
              />

              <FormField
                className="formField"
                id={"password"}
                formdata={this.state.formdata.password}
                change={element => this.updateForm(element)}
              />
              <Button
                style={{
                  backgroundColor: "#17394d",
                  color: "#fff",
                  margin: 10
                }}
                size="large"
                onClick={event => this.submitForm(event)}
              >
                Log in
              </Button>
              <Link to="/signup">
                <Button
                  style={{
                    margin: 10
                  }}
                  color="inherit"
                  size="large"
                >
                  Sign Up
                </Button>
              </Link>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default connect()(Login);
