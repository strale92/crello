import React, { Component } from "react";
import { validate } from "../UI/misc";
import FormField from "../UI/formField";
import Button from "@material-ui/core/Button";
import { connect } from "react-redux";
import { handleSignUp } from "../../actions/login.actions";
import { Link } from "react-router-dom";

class SignUp extends Component {
  state = {
    // formError: false,
    // formSuccess: "",
    formdata: {
      name: {
        element: "input",
        value: "",
        config: {
          name: "name_input",
          type: "text",
          placeholder: "Enter your name"
        },
        validation: {
          required: true
        },
        valid: false,
        validationMessage: ""
      },
      lastname: {
        element: "input",
        value: "",
        config: {
          name: "lastname_input",
          type: "text",
          placeholder: "Enter your lastname"
        },
        validation: {
          required: true
        },
        valid: false,
        validationMessage: ""
      },
      username: {
        element: "input",
        value: "",
        config: {
          field: "username",
          name: "username_input",
          type: "text",
          placeholder: "Enter your username"
        },
        validation: {
          required: true
        },
        valid: false,
        validationMessage: ""
      },
      password: {
        element: "input",
        value: "",
        config: {
          name: "password_input",
          type: "password",
          placeholder: "Enter your password"
        },
        validation: {
          required: true
        },
        valid: false,
        validationMessage: ""
      },
      repeatedPassword: {
        element: "input",
        value: "",
        config: {
          name: "repeated_password_input",
          type: "password",
          placeholder: "Enter your password again"
        },
        validation: {
          required: true
        },
        valid: false,
        validationMessage: ""
      }
    }
  };

  customValidation(password, repeatedPassword) {
    let error = [true, ""];
    console.log("p", password);
    console.log("rp", repeatedPassword);
    const valid = password === repeatedPassword;
    const message = `${!valid ? "The passwords must be same" : ""}`;
    error = !valid ? [valid, message] : error;

    return error;
  }

  updateForm(element) {
    const newFormdata = { ...this.state.formdata };
    const newElement = { ...newFormdata[element.id] };

    newElement.value = element.event.target.value;

    if (element.id === "repeatedPassword") {
      let validData = this.customValidation(
        this.state.formdata.password.value,
        newElement.value
      );
      newElement.valid = validData[0] && newElement.value.length > 0;
      newElement.validationMessage = validData[1];
      newFormdata[element.id] = newElement;
    } else {
      let validData = validate(newElement);

      newElement.valid = validData[0];
      newElement.validationMessage = validData[1];

      newFormdata[element.id] = newElement;
    }

    this.setState({ formError: false, formdata: newFormdata });
  }

  submitForm(event) {
    event.preventDefault();

    let dataToSubmit = {};
    let formIsValid = true;

    for (let key in this.state.formdata) {
      dataToSubmit[key] = this.state.formdata[key].value;
      formIsValid = this.state.formdata[key].valid && formIsValid;
    }

    if (formIsValid) {
      this.props.dispatch(
        handleSignUp(
          dataToSubmit.name,
          dataToSubmit.lastname,
          dataToSubmit.username,
          dataToSubmit.password,
          dataToSubmit.repeatedPassword
        )
      );
    }
  }

  render() {
    return (
      <div className="landing">
        <h1>Crello</h1>

        <div className="signin_wrapper" style={{ margin: "100px" }}>
          <form onSubmit={event => this.submitForm()}>
            <h2>Please enter informations for your profil</h2>
            <FormField
              className="formField"
              id={"name"}
              formdata={this.state.formdata.name}
              change={element => this.updateForm(element)}
            />

            <FormField
              className="formField"
              id={"lastname"}
              formdata={this.state.formdata.lastname}
              change={element => this.updateForm(element)}
            />
            <FormField
              className="formField"
              id={"username"}
              formdata={this.state.formdata.username}
              change={element => this.updateForm(element)}
            />
            <FormField
              className="formField"
              id={"password"}
              formdata={this.state.formdata.password}
              change={element => this.updateForm(element)}
            />
            <FormField
              className="formField"
              id={"repeatedPassword"}
              formdata={this.state.formdata.repeatedPassword}
              change={element => this.updateForm(element)}
            />
            <Button
              style={{
                backgroundColor: "#17394d",
                color: "#fff",
                margin: 10
              }}
              size="large"
              onClick={event => this.submitForm(event)}
            >
              Sign Up
            </Button>
            <Link to="/login">
              <Button
                style={{
                  margin: 10
                }}
                color="inherit"
                size="large"
              >
                Back to Login page
              </Button>
            </Link>
          </form>
        </div>
      </div>
    );
  }
}

export default connect()(SignUp);
