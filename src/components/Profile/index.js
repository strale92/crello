import React, { Component } from "react";
import { connect } from "react-redux";
import CountUp from "react-countup";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import HeaderImage from "../../assets/images/2.jpg";
let sum = 0;

class Profile extends Component {
  dashboardsCount = () => {
    sum = 0;
    sum += this.props.dashboards.length;
    return (
      <CountUp start={0} end={this.props.dashboards.length} duration={2} />
    );
  };

  taskListCount = () => {
    let counter = 0;
    this.props.dashboards.forEach(dashboard => {
      counter += dashboard.taskStates.length;
    });
    sum += counter;
    return <CountUp start={0} end={counter} duration={2} />;
  };

  tasksCount = () => {
    let counter = 0;
    this.props.dashboards.forEach(dashboard => {
      dashboard.taskStates.forEach(taskState => {
        counter += taskState.tasks.length;
      });
    });
    sum += counter;

    return <CountUp start={0} end={counter} duration={2} />;
  };

  showStatistics = () => {
    return (
      <div className="profile-stats">
        <div className="stats">
          <div className="top-stat">Dashboards:</div>
          <div className="bottom-stat">{this.dashboardsCount()}</div>
        </div>
        <div className="stats">
          <div className="top-stat">TaskLists:</div>
          <div className="bottom-stat"> {this.taskListCount()}</div>
        </div>
        <div className="stats">
          <div className="top-stat">Tasks:</div>{" "}
          <div className="bottom-stat"> {this.tasksCount()}</div>
        </div>
      </div>
    );
  };

  showLevel() {
    if (sum < 20) {
      return (
        <div>
          <div style={{ fontSize: 18, margin: "0 5px 10px 5px" }}>
            Begginer:
          </div>
          <i
            style={{ color: "rgb(220, 208, 192)", fontSize: 50 }}
            className="fas fa-star"
          />
        </div>
      );
    } else if (sum < 60) {
      return (
        <div>
          <div style={{ fontSize: 18, margin: "0 5px 10px 5px" }}>
            Experienced user:
          </div>
          <i
            style={{ color: "rgb(220, 208, 192)", fontSize: 50 }}
            className="fas fa-star"
          />
          <i
            style={{ color: "rgb(220, 208, 192)", fontSize: 50 }}
            className="fas fa-star"
          />
        </div>
      );
    }
  }

  card = () => {
    return (
      <Card style={{ width: "90%", margin: 15 }}>
        <CardMedia style={{ height: 200 }} image={HeaderImage} />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            Your info:
          </Typography>
          <Typography
            component="div"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <div className="profile-info">
              <div className="profile-info-child">
                <strong>Name:</strong> {this.props.user.name}
              </div>
              <div className="profile-info-child">
                <strong>Lastname:</strong> {this.props.user.lastName}
              </div>
            </div>
          </Typography>
          <Typography gutterBottom variant="h5" component="h2">
            Your statistics:
          </Typography>
          <Typography
            component="div"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            {this.showStatistics()}
          </Typography>
          <Typography gutterBottom variant="h5" component="h2">
            Your{" "}
            <span style={{ fontFamily: "Patrick Hand", fontSize: 24 }}>
              Crello
            </span>{" "}
            level:
          </Typography>
          <Typography
            component="div"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            {this.showLevel()}
          </Typography>
        </CardContent>
      </Card>
    );
  };

  render() {
    console.log("sum", sum);
    return (
      <div className="container">
        {this.props.dashboards ? this.card() : null}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { dashboards, authentication } = state;
  console.log(dashboards);
  const { user } = authentication;
  return {
    dashboards,
    user
  };
}

export default connect(mapStateToProps)(Profile);
